﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CardGameWar
{
    public partial class GameScreen : Form
    {
        public const int NumberOfCards = 54;
        public const int CardsOnDeck = 10;

        public GameScreen()
        {
            InitializeComponent();
        }

        public void GenerateCards()
        {
            PictureBox[] Cards = new PictureBox[CardsOnDeck];
            //create 10 red back cards
            for(int i = 0; i < CardsOnDeck; i++)
            {
                Cards[i] = new PictureBox();
                Cards[i].SizeMode = PictureBoxSizeMode.StretchImage;
                Cards[i].Size = new Size(60, 78);
                Cards[i].BackgroundImage = Properties.Resources.card_back_red;
                Cards[i].BackgroundImageLayout = ImageLayout.Center;
                Cards[i].Image = Properties.Resources.card_back_red;
                if (i == 0)
                {
                    Cards[i].Location = new Point(59, 244);
                }
                else
                {
                    Cards[i].Location = new Point(Cards[i-1].Location.X + 66, 244);
                }
                Cards[i].Anchor = AnchorStyles.Left;
                Cards[i].Visible = true;
                this.Controls.Add(Cards[i]);
            }
            //create opponent's blue back card
            PictureBox blueCard = new PictureBox();
            blueCard.SizeMode = PictureBoxSizeMode.StretchImage;
            blueCard.Size = new Size(60, 78);
            blueCard.BackgroundImage = Properties.Resources.card_back_blue;
            blueCard.BackgroundImageLayout = ImageLayout.Center;
            blueCard.Image = Properties.Resources.card_back_blue;
            blueCard.Location = new Point(355, 76);
            blueCard.Anchor = AnchorStyles.Left;
            blueCard.Visible = true;
            this.Controls.Add(blueCard);
        }

        private void GameScreen_Load(object sender, EventArgs e)
        {
            GenerateCards();
        }

        private void button1_MouseDown(object sender, MouseEventArgs e)
        {
            Application.Exit();
        }
    }
}
